//Function make strings comparable
function getValueForRow(tr, colID) {
    let val = tr.cells[colID].innerHTML;
    if (colID === 0 || colID === 4) {
        val = Number(val);
    } else {
        val = val.toLowerCase()
    }
    return val;
}

//Function sort table ascending
function sortTable(colID) {
    // comparing function, used in Array.sort
    function compareTwoLines(tr1, tr2) {
        let val1 = getValueForRow(tr1, colID);
        let val2 = getValueForRow(tr2, colID);
        if (val1 < val2) {
            return -1;
        } else if (val1 > val2) {
            return 1;
        } else {
            return 0;
        }
    }
    // Let's check the icon class (the direction to order in)
    //fas fa-sort-up
    let icons = document.querySelectorAll("#myTable thead th i");
    let icon = icons[colID];
    let dir = 'asc';
    if (icon.classList.contains('fa-sort-up')) {
        dir = 'desc';
    }
    let tbody = document.querySelector('#myTable tbody');
    // document.querySelectorAll('#myTable tbody tr')
    var trs = Array.from($('#myTable tbody tr'));
    // let's use Array's sort method
    trs.sort(compareTwoLines);
    if (dir === 'desc') {
        trs.reverse();
    }
    // trs are sorted now, let's put them back into the DOM.
    for (let tr of trs) {
        // because the tr's were already existing, they are now just reordered
        // according to the document
        tbody.appendChild(tr);
    }
    // How to decide if ascending or descending sorting
    for (let i = 0; i < icons.length; i++) {
        icons[i].classList.replace('fa-sort-up', 'fa-sort');
        icons[i].classList.replace('fa-sort-down', 'fa-sort');
    }
    if (dir === 'desc') {
        icon.classList.replace('fa-sort', 'fa-sort-down');
    } else {
        icon.classList.replace('fa-sort', 'fa-sort-up');
    }
}


//Function to append new rows to the table
function appendRow(rowData) {
    const table = document.getElementById("myTable");
    let row = table.insertRow(table.rows.length);
    var attr = ['id', 'first', 'last', 'formation', 'score'];
    for (var i = 0; i < 5; i++) {
        let cell = row.insertCell(i);
        cell.innerHTML = rowData[attr[i]];
    }
}


$(document).ready(function () {

    let whiteList = {
        input: ['placeholder', 'aria-label', 'required', 'pattern'],
        select: ['required'],
        option: [],
        button: []
    };
    // Add the default whitelist's properties to our whitelists
    Object.assign(whiteList, $.fn.popover.Constructor.Default.whiteList);

    // initialize the popover
    $("#add-row-popover").popover({
        html: true,
        title: 'Enter new student&prime;s data',
        whiteList: whiteList,
        content: $('#add-row-popover-content')[0].innerHTML
    });

    document.addEventListener('click', function (evt) {
        if (evt.target.id === 'add-row-save') {
            //check validity of arguments
            let inputGroups = $(evt.target.parentNode).find('.input-group');
            inputGroups.addClass('was-validated');


            // check the validity of the form inputs
            let constraintElements = inputGroups.find('[required]');
            for (let e of constraintElements) {
                let isValid = e.checkValidity();
                if (!isValid) {
                    return; // don't do anything anymore, input not ok, so not adding to table
                }
            };

            $("#add-row-popover").popover('hide');
            let ret = {};
            for (let input of constraintElements) {
                if (input.id.startsWith('add-row-')) {
                    ret[input.id.substring(8)] = $(input).val();
                }
            }
            console.log(ret);
            appendRow(ret);
        }
    });
});


//Dropdown Menu
$(document).ready(function () {
    $('select').on('change', function () {
        let theResult = '';
        let val = $(this).val();
        switch (val) {
            case 'Student Best Score':
                theResult = StudentBestScore();
                break;
            case 'Student Worst Score':
                theResult = StudentWorstScore();
                break;
            case 'Average Score':
                theResult = AverageScore();
                break;
            case 'Amount Students':
                theResult = AmountStudents();
                break;
        }
        $('#result').text(val + ": " + theResult);
    });
});

//General functions for Dropdown Menu
function getScore(tr) {
    return Number(tr.cells[4].innerHTML);
}
function getFullName(tr) {
    return tr.cells[1].innerHTML + " " + tr.cells[2].innerHTML;
}

//Specific functions dropdown menu
function StudentBestScore() {
    const table = document.getElementById("myTable");
    let minVal = getScore(table.rows[1]);
    let rowIndex = 1;
    for (var i = 2; i < table.rows.length; i++) {
        let nextRow = getScore(table.rows[i]);
        if (minVal < nextRow) {
            minVal = nextRow;
            rowIndex = i;
        }
    }
    return getFullName(table.rows[rowIndex]);
}

function StudentWorstScore() {
    const table = document.getElementById("myTable");
    let minVal = getScore(table.rows[1]);
    let rowIndex = 1;
    for (var i = 2; i < table.rows.length; i++) {
        let nextRow = getScore(table.rows[i]);
        if (minVal > nextRow) {
            minVal = nextRow;
            rowIndex = i;
        }
    }
    return getFullName(table.rows[rowIndex]);
}

function AverageScore() {
    const table = document.getElementById("myTable");
    let sum = 0;
    for (var i = 1; table.rows.length > i; i++) {
        sum = sum + getScore(table.rows[i]);
    }
    return (sum / AmountStudents());
}

function AmountStudents() {
    const sum = $('#myTable tbody tr').length;
    return sum;
}

